use lazy_static::lazy_static;
use regex::Regex;
use std::{
    collections::{HashSet, hash_set::Iter},
    iter::Iterator,
    io::{self, BufRead}, ops::{Add, Sub}, fmt, mem,
};

fn main() {
    let stdin = io::stdin();
    let lines = stdin.lock().lines()
        .map(|l| l.expect("error reading line"));

    let states = commands_with_history(lines);
    for state in states.iter() {
        println!("{} marked", state.len());
    }
    println!("{}", states.last().unwrap());
}

#[derive(Debug, Clone, PartialEq, Eq, Default)]
struct Board {
    markings: HashSet<(usize, usize)>,
}

impl Board {
    pub fn insert(&mut self, elem: (usize, usize)) -> bool {
        self.markings.insert(elem)
    }

    pub fn paper_fold(&self, along: FoldAlong, fold_at: usize) -> Board {
        use FoldAlong::*;
        return Self {
            markings: self
                .iter()
                .map(|(x, y)| match along {
                    X => (fold_down(*x, fold_at), *y),
                    Y => (*x, fold_down(*y, fold_at)),
                })
                .collect::<HashSet<_>>() ,
        }
    }

    pub fn iter(&self) -> Iter<(usize, usize)> {
        self.markings.iter()
    }

    pub fn len(&self) -> usize {
        self.markings.len()
    }
}

impl fmt::Display for Board {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let max_x = *self.markings.iter().map(|(x, _)| x).max().unwrap_or(&0);
        let max_y = *self.markings.iter().map(|(_, y)| y).max().unwrap_or(&0);
        for i in 0..max_y + 1 {
            for j in 0..max_x + 1 {
                write!(f, "{}", if self.markings.contains(&(j, i)) { "#" } else { "." })?;
            }
            writeln!(f)?;
        }
        Ok(())
    }
}

fn apply_command_inplace(coords: &mut Board, command: Command) {
    use Command::*;
    match command {
        Nop => {},
        Coordinate { x, y } => {
            if !coords.insert((x, y)) { println!("Warning: duplicate entry {:?}", (x, y))};
        }
        Fold { along,  value: fold_at } => {
            mem::swap(coords, &mut coords.paper_fold(along, fold_at));
        }
    };
}

fn commands_with_history<I>(lines: I) -> Vec<Board>
        where I: Iterator<Item = String> {
    lines
        .map(|l| parse_line(&l).unwrap())
        .collect::<Vec<_>>()
        .iter()
        .scan(
            Board::default(),
            |b, &c| {
                apply_command_inplace(b, c);
                match c {
                    Command::Fold { along: _, value: _ } => Some(Some(b.clone())),
                    _ => Some(None),
                }
            }
        )
        .flatten()
        .collect::<Vec<_>>()
}


#[derive(Clone, Copy, Debug, PartialEq, Eq)]
enum FoldAlong {
    X,
    Y,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
enum Command {
    Nop,
    Coordinate { x: usize, y: usize },
    Fold { along: FoldAlong, value: usize },
}

fn parse_line(line: &str) -> Result<Command, String> {
    use Command::*;
    let trimmed = line.trim();
    lazy_static! {
        static ref RE_FOLD: Regex = Regex::new(r"fold along (x|y)=(\d+)").unwrap();
        static ref RE_COORD: Regex = Regex::new(r"(\d+),(\d+)").unwrap();
    }

    if trimmed.is_empty() {
        Ok(Nop)
    } else if let Some(cap) = RE_FOLD.captures(trimmed) {
        Ok(Fold {
            value: cap[2].parse::<usize>().expect("Invalid integer"),
            along: match &cap[1] {
                "x" => FoldAlong::X,
                "y" => FoldAlong::Y,
                _ => panic!("Regex RE_COORD is broken: matched something other than (x|y)"),
            },
        })
    } else if let Some(cap) = RE_COORD.captures(trimmed) {
        Ok(Coordinate {
            x: cap[1].parse::<usize>().expect("Invalid integer"),
            y: cap[2].parse::<usize>().expect("Invalid integer"),
        })
    } else {
        Err(format!("Invalid command: {:?}", line))
    }
}

fn fold_down<N>(number: N, fold_at: N) -> N
    where N: Copy + Ord + Add + Sub<Output = N> {
    if number <= fold_at {
        number
    } else {
        fold_at - (number - fold_at)
    }
}

#[cfg(test)]
mod tests {
    use rstest::rstest;

    use super::*;
    use Command::*;

    #[rstest]
    #[case("5,7", Coordinate { x: 5, y: 7 })]
    #[case("  ", Nop)]
    #[case("  \t ", Nop)]
    #[case("fold along x=5", Fold { along: FoldAlong::X, value: 5 })]
    #[case("fold along y=7", Fold { along: FoldAlong::Y, value: 7 })]
    fn test_parse_good_lines(#[case] line: &str, #[case] expected: Command) {
        assert_eq!(parse_line(line), Ok(expected));
    }

    #[rstest]
    #[case("blah")]
    fn test_parse_bad_lines(#[case] line: &str) {
        assert!(parse_line(line).is_err());
    }

    #[test]
    fn test_example() {
        use indoc::indoc;
        let lines= indoc! {
            "6,10
            0,14
            9,10
            0,3
            10,4
            4,11
            6,0
            6,12
            4,1
            0,13
            10,12
            3,4
            3,0
            8,4
            1,10
            2,14
            8,10
            9,0

            fold along y=7
            fold along x=5"
        }.lines().map(|s| s.to_string());

        let states = commands_with_history(lines);
        for state in states.iter() {
            println!("{}", state);
        }

        assert_eq!(states.iter().map(|b| b.len()).collect::<Vec<_>>(), vec![17, 16]);
        assert_eq!(
            format!("{}", states.last().unwrap()),
            indoc! { "
                #####
                #...#
                #...#
                #...#
                #####
                .....
                .....
                "
            }
        );
    }
}