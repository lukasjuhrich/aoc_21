use std::io::BufRead;
use std::io;

fn main() {
    let stdin = io::stdin();
    let ints: Vec<u32> = stdin.lock().lines()
        .map(|l| l.expect("Error reading line :<"))
        .map(|l| l
            .parse::<u32>()
            .unwrap_or_else(|_| panic!("no number: '{}'", l))
        )
        .collect();
    println!("result: {}", sum_of_increases(&ints));

    let triples = ints.iter().zip(ints.iter().skip(1))
        .zip(ints.iter().skip(2)).map(|((a, b), c)| a + b + c).collect::<Vec<u32>>();
    println!("tri-window sum: {}", sum_of_increases(&triples));
}

fn sum_of_increases(i: &[u32]) -> u32 {
    return i.iter().zip(i.iter().skip(1))
        .map(|(pre, post)| if pre < post {1u32} else {0u32})
        .sum();
}