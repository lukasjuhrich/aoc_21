use std::{io::{self, BufRead}, ops::Mul};

fn main() {
    let stdin = io::stdin();
    let commands = stdin.lock().lines()
        .map(|l| l.expect("Error reading line"))
        .map(Command::parse_str)
        .collect::<Result<Vec<Command>, &str>>()
        .expect("Something went wrong when parsing")
    ;

    let pos_without_aim = commands
        .iter()
        .fold((0, 0), step_without_aim)
    ;
    println!(
        "Sum (forward, depth) is {:#?}, product is {}",
        pos_without_aim, pos_without_aim.0 * pos_without_aim.1 as u64,
    );

    let pos_with_aim = commands
        .iter()
        .fold(State::default(), step_with_aim)
    ;
    println!(
        "Last state: {:#?}, prod: {}",
        pos_with_aim, pos_with_aim.horiz * pos_with_aim.depth as u64
    )
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
enum Command {
    Forward(u64),
    Up(u64),
    Down(u64)
}

impl Command {
    fn parse_str(s: String) -> Result<Self, &'static str> {
        let value =
            s
            .split(' ')
            .nth(1)
            .ok_or("Invalid command")?
            .parse::<u64>()
            .map_err(|_| "Invalid integer value")?;

        if s.starts_with("forward ") {
            return Ok(Self::Forward(value));
        }
        if s.starts_with("down ") {
            return Ok(Self::Down(value));
        }
        if s.starts_with("up ") {
            return Ok(Self::Up(value));
        }

        Err("Command must start with `(forward|up|down) `!")
    }
}


fn step_without_aim(state: (u64, i64), command: &Command) -> (u64, i64) {
    let (horiz, depth) = state;
    match command {
        Command::Up(val) => (horiz, depth - *val as i64),
        Command::Down(val) => (horiz, depth + *val as i64),
        Command::Forward(val) => (horiz + *val, depth),
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Default)]
struct State {
    horiz: u64,
    depth: i64,
    aim: i64,
}

impl State {
    fn with_delta(&self, d_horiz: u64, d_depth: i64, d_aim: i64) -> State {
        State {
            horiz: self.horiz + d_horiz,
            depth: self.depth + d_depth,
            aim: self.aim + d_aim,
        }
    }
}

fn step_with_aim(state: State, command: &Command) -> State {
    match command {
        Command::Up(val) => state.with_delta(0, 0, -(*val as i64)),
        Command::Down(val) => state.with_delta(0, 0, *val as i64),
        Command::Forward(val) => state.with_delta(*val, state.aim.mul(*val as i64), 0),
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_parse_str() {
        assert_eq!(
            Command::parse_str("down 5".to_string()),
            Ok(Command::Down(5)),
        );
    }

    #[test]
    fn test_part_2() {
        let commands = vec![
            Command::Forward(5),
            Command::Down(5),
            Command::Forward(8),
            Command::Up(3),
            Command::Down(8),
            Command::Forward(2),
        ];
        let state: State = commands.iter().fold(State::default(), step_with_aim);
        assert_eq!(state.horiz, 15);
        assert_eq!(state.depth, 60);
    }
}
