use std::io;
use std::io::BufRead;

fn main() {
    let stdin = io::stdin();
    let lines: Vec<String> = stdin
        .lock()
        .lines()
        .map(|l| l.expect("Error reading line :<"))
        .collect();

    let check_results = lines
        .iter()
        .map(|l| syntax_check(l.as_str()))
        .collect::<Vec<_>>();

    let total_penalty = check_results
        .iter()
        .filter_map(|r| Some(r.as_ref().err()?.penalty())) // personal taste, just another option
        .sum::<u64>();
    println!("Total penalty: {}", total_penalty);

    let mut completion_scores =
        check_results
        .iter()
        .filter_map(|r| Some(r.as_ref().ok()?.as_slice()))
        .map(line_completion_points)
        .collect::<Vec<u64>>();
    completion_scores.sort_unstable();
    if completion_scores.len() % 2 == 0 { panic!("Expected an even number of good scores!")};
    let middle_score = completion_scores[(completion_scores.len()-1)/2];

    println!("Middle completion score: {}", middle_score);
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
enum Bracket {
    Square,
    Angled,
    Paren,
    Brace,
}

impl Bracket {
    #![allow(dead_code)]
    fn closing_char(self) -> char {
        use Bracket::*;
        match self {
            Square => ']',
            Paren => ')',
            Angled => '>',
            Brace => '}',
        }
    }

    fn completion_points(self) -> u64 {
        use Bracket::*;
        match self {
            Paren => 1,
            Square => 2,
            Brace => 3,
            Angled => 4,
        }
    }

    fn penalty(self) -> u64 {
        use Bracket::*;
        match self {
            Paren => 3,
            Square => 57,
            Brace => 1197,
            Angled => 25137,
        }
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
enum Orientation {
    Opening,
    Closing,
}

fn parse_char(c: char) -> Option<(Bracket, Orientation)> {
    Some(match c {
        '<' => (Bracket::Angled, Orientation::Opening),
        '[' => (Bracket::Square, Orientation::Opening),
        '{' => (Bracket::Brace, Orientation::Opening),
        '(' => (Bracket::Paren, Orientation::Opening),
        '>' => (Bracket::Angled, Orientation::Closing),
        ']' => (Bracket::Square, Orientation::Closing),
        '}' => (Bracket::Brace, Orientation::Closing),
        ')' => (Bracket::Paren, Orientation::Closing),
        _ => return None,
    })
}

/// Processes the state change signified by an element (thing, orientation).
///
/// If an incorrect bracket is provided, return an error with the bracket kind.
fn process_and_verify(
    mut stack: Vec<Bracket>,
    el: (Bracket, Orientation),
) -> Result<Vec<Bracket>, Bracket> {
    use Orientation::*;
    let (bracket, orientation) = el;

    match orientation {
        Opening => {
            stack.push(bracket);
            Ok(stack)
        }
        Closing => {
            if let Some(expected_paren) = stack.pop() {
                if expected_paren == bracket {
                    return Ok(stack);
                }
            };
            Err(bracket)
        }
    }
}

fn syntax_check(line: &str) -> Result<Vec<Bracket>, Bracket> {
    let stack = Vec::<Bracket>::new();
    line.chars()
        .map(|c| parse_char(c).expect("Invalid char"))
        .fold(Ok(stack), |stack_res, el|
            process_and_verify(stack_res?, el)
        )
}

fn line_completion_points(stack: &[Bracket]) -> u64 {
    stack.iter()
        .rev()
        .fold(0, |score, bracket| score * 5 + bracket.completion_points()) // Are you sure about the bounds? Otherwise checked arithmetic would be a wise choice
}

#[cfg(test)]
mod test {
    use super::*;
    use rstest::rstest;
    use Bracket::*;
    use Orientation::*;

    #[rstest]
    #[case('<', Some((Angled, Opening)))]
    #[case('[', Some((Square, Opening)))]
    #[case('}', Some((Brace, Closing)))]
    #[case(')', Some((Paren, Closing)))]
    #[case('f', None)]
    fn test_char_parsing(#[case] c: char, #[case] expected: Option<(Bracket, Orientation)>) {
        assert_eq!(parse_char(c), expected);
    }

    #[rstest]
    #[case("(]", Bracket::Square)]
    fn test_line_syntax_check_errors(#[case] line: &str, #[case] expected: Bracket) {
        assert_eq!(syntax_check(line), Err(expected))
    }

    #[rstest]
    #[case("([]{()}", ")")]
    #[case("([", "])")]
    #[case("[({(<(())[]>[[{[]{<()<>>", "}}]])})]")]
    fn test_line_syntax_check_stack(#[case] line: &str, #[case] expected_rest: &str) {
        let res = syntax_check(line);
        assert!(res.is_ok());
        let stack = res.unwrap();
        let actual_rest: String = stack.iter().map(|&b| b.closing_char()).rev().collect();
        assert_eq!(actual_rest, expected_rest);
    }

    #[rstest]
    #[case("[({(<(())[]>[[{[]{<()<>>", 288957)]
    fn test_completion_points(#[case] line: &str, #[case] expected_points: u64) {
        let stack = syntax_check(line).unwrap();
        assert_eq!(line_completion_points(&stack), expected_points);
    }
}
