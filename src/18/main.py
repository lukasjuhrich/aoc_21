from copy import deepcopy
from functools import reduce
from itertools import dropwhile, permutations

import pytest

def iter_with_path(num, path_til_now=None):
    assert num is not None
    if not path_til_now: path_til_now = []
    if isinstance(num, int):
        yield path_til_now, num
        return
    for i in (0, 1):
        yield from iter_with_path(num[i], path_til_now + [i])


def l(n, p):
    left_branch = list(dropwhile(lambda x: x == 0, p[::-1]))[::-1][:-1] + [0]
    left_sub = visit(n, left_branch)
    if isinstance(left_sub, int):
        return left_branch
    last_path, _ = list(iter_with_path(left_sub, left_branch))[-1]
    return last_path


def r(n, p):
    right_branch = list(dropwhile(lambda x: x == 1, p[::-1]))[::-1][:-1] + [1]
    right_sub = visit(n, right_branch)
    if isinstance(right_sub, int):
        return right_branch
    first_path, _ = next(iter_with_path(right_sub, right_branch))
    return first_path


def visit(n, p): return reduce(lambda x, i: x[i], p, n)
def set_val(n, p, val): visit(n, p[:-1])[p[-1]] = val
def add(n, p, val): visit(n, p[:-1])[p[-1]] = visit(n, p) + val


def pair_to_explode(n):
    assert n is not None
    return next((p[:-1] for p, _ in iter_with_path(n) if len(p) == 5), None)


def explode(n, p):
    lval, rval = visit(n, p)
    if 0 in p:
        # we can explode rightwards
        add(n, r(n, p), rval)
    if 1 in p:
        # we can explode leftwards
        add(n, l(n, p), lval)
    set_val(n, p, 0)


def num_to_split(n):
    return next((p for p, num in iter_with_path(n) if num >= 10), None)


def split(n, p):
    val = visit(n, p)
    set_val(n, p, [val // 2, val - (val // 2)])


def process(n):
    while True:
        if (p := pair_to_explode(n)) is not None:
            assert isinstance(visit(n, p), list), f"{n}, {p}"
            explode(n, p)
            continue
        if (p := num_to_split(n)) is not None:
            assert isinstance(visit(n, p), int)
            split(n, p)
            continue
        return n


def fish_sum(lines):
    parsed = [eval(l) for l in lines if l.strip()]
    return reduce(lambda a, b: process([a, b]), parsed)


TEST_LINES = """\
[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]
[[[5,[2,8]],4],[5,[[9,9],0]]]
[6,[[[6,2],[5,6]],[[7,6],[4,7]]]]
[[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]
[[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]
[[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]
[[[[5,4],[7,7]],8],[[8,3],8]]
[[9,3],[[9,9],[6,[4,9]]]]
[[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]
[[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]
""".splitlines()

@pytest.mark.parametrize('inp, expected', [
    ("[1,1] [2,2]", '[[1,1],[2,2]]'),
    ("[1,1] [2,2] [3,3] [4,4]",'[[[[1,1],[2,2]],[3,3]],[4,4]]'),
    ("[1,1] [2,2] [3,3] [4,4] [5,5] [6,6]", '[[[[5,0],[7,4]],[5,5]],[6,6]]'),
    (" ".join(TEST_LINES), '[[[[6,6],[7,6]],[[7,7],[7,0]]],[[[7,7],[7,7]],[[7,8],[9,9]]]]'),
])
def test_sums(inp, expected):
    assert fish_sum(inp.split()) == eval(expected)


def magnitude(n):
    if isinstance(n, int): return n
    return 3*magnitude(n[0]) + 2*magnitude(n[1])


def largest_mag_of_two_sums(lines):
    return max(magnitude(process([deepcopy(one), deepcopy(other)]))
               for one, other in permutations(lines, 2))


def test_largest_mag():
    inp = [eval(l) for l in TEST_LINES]
    assert largest_mag_of_two_sums(inp) == 3993


if __name__ == '__main__':
    import sys
    lines = sys.stdin.readlines()
    processed = fish_sum(lines)
    print(processed)
    print(magnitude(processed))
    print(largest_mag_of_two_sums([eval(l) for l in lines]))

