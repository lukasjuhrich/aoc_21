#[allow(clippy::all)]

use std::collections::{HashMap, HashSet};
use std::io;
use std::io::BufRead;
use std::str::FromStr;

fn main() {
    let stdin = io::stdin();
    println!();
    let lines = stdin.lock().lines()
        .map(|l| l.expect("failed to read line"))
        .collect::<Vec<String>>();

    todo!("Parse lines into nums / list of boards");
}

struct Board {
    // number N is at pos (x, y)
    num_pos_map: HashMap<u8, (u8, u8)>,

    // Is the number at position (x, y) marked?
    markings: HashSet<(u8, u8)>,

    unchecked_fields_per_row: HashSet<(u8, u8)>,
    unchecked_fields_per_col: HashSet<(u8, u8)>,
}

impl FromStr for Board {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        todo!()
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use indoc::indoc;

    #[test]
    fn test_parsing() {
        let board_input= indoc! {
            "22 13 17 11  0
            8  2 23  4 24
            21  9 14 16  7
            6 10  3 18  5
            1 12 20 15 19"
        };
        let board = board_input.parse::<Board>();
    }
}
