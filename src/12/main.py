import collections as c

def parse(data):
    d = c.defaultdict(list)
    for line in data.splitlines():
        a, b = line.strip().split('-', maxsplit=1)
        d[a].append(b)
        d[b].append(a)
    return d


def can_visit_a(n, path):
    return n not in path or n.isupper()


def can_visit_b(n, path):
    if n not in path or n.isupper():
        return True
    if n == 'start':
        return False
    # n in path: only allowed if we don't have lower double visits yet
    visited = set()
    for p in path:
        if p.islower():
            if p in visited:
                return False
            visited.add(p)
    return True


def iter_paths(adj, path, can_visit):
    if not path:
        yield from iter_paths(adj, ['start'], can_visit)
        return
    last = path[-1]
    if last == 'end':
        yield path
        return
    for n in adj[path[-1]]:
        if not can_visit(n, path):
            continue
        yield from iter_paths(adj, [*path, n], can_visit)

def paths(adj, *a):
    return {",".join(p) for p in iter_paths(adj, [], *a)}


SMALL_INPUT = """\
    start-A
    start-b
    A-c
    A-b
    b-d
    A-end
    b-end\
"""

def test_small_input():
    expected = """\
        start,A,b,A,c,A,end
        start,A,b,A,end
        start,A,b,end
        start,A,c,A,b,A,end
        start,A,c,A,b,end
        start,A,c,A,end
        start,A,end
        start,b,A,c,A,end
        start,b,A,end
        start,b,end\
    """
    adj = parse(SMALL_INPUT)
    assert paths(adj, can_visit_a) == {
        x.strip() for x in expected.splitlines()
    }

LARGE_INPUT = """\
    fs-end
    he-DX
    fs-he
    start-DX
    pj-DX
    end-zg
    zg-sl
    zg-pj
    pj-he
    RW-he
    fs-DX
    pj-RW
    zg-RW
    start-pj
    he-WI
    zg-he
    pj-fs
    start-RW\
"""

def test_large_input():
    adj = parse(LARGE_INPUT)
    assert len(paths(adj, can_visit_a)) == 226


def test_small_b():
    expected = """\
        start,A,b,A,b,A,c,A,end
        start,A,b,A,b,A,end
        start,A,b,A,b,end
        start,A,b,A,c,A,b,A,end
        start,A,b,A,c,A,b,end
        start,A,b,A,c,A,c,A,end
        start,A,b,A,c,A,end
        start,A,b,A,end
        start,A,b,d,b,A,c,A,end
        start,A,b,d,b,A,end
        start,A,b,d,b,end
        start,A,b,end
        start,A,c,A,b,A,b,A,end
        start,A,c,A,b,A,b,end
        start,A,c,A,b,A,c,A,end
        start,A,c,A,b,A,end
        start,A,c,A,b,d,b,A,end
        start,A,c,A,b,d,b,end
        start,A,c,A,b,end
        start,A,c,A,c,A,b,A,end
        start,A,c,A,c,A,b,end
        start,A,c,A,c,A,end
        start,A,c,A,end
        start,A,end
        start,b,A,b,A,c,A,end
        start,b,A,b,A,end
        start,b,A,b,end
        start,b,A,c,A,b,A,end
        start,b,A,c,A,b,end
        start,b,A,c,A,c,A,end
        start,b,A,c,A,end
        start,b,A,end
        start,b,d,b,A,c,A,end
        start,b,d,b,A,end
        start,b,d,b,end
        start,b,end\
    """
    adj = parse(SMALL_INPUT)
    assert paths(adj, can_visit_b) == {x.strip() for x in expected.splitlines()}


def test_large_b():
    adj = parse(LARGE_INPUT)
    assert len(paths(adj, can_visit_b)) == 3509

if __name__ == '__main__':
    import sys
    variant = can_visit_b if len(sys.argv) >= 2 and sys.argv[1] == 'b' else can_visit_a
    adj = parse(sys.stdin.read())
    for p in paths(adj, variant):
        print(p)
